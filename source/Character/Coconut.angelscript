#include "Bullet.angelscript"

class Coconut : Bullet
{
	private ETHEntity@ m_entity;
	private float m_moveSpeed = 800.0f;
	private FrameTimer m_frameTimer;

	private vector2 m_direction;

	private uint frameLine;
	private uint frameCol;

	Coconut(const vector2 &in position = vector2(0.0f, 0.0f), 
		const vector2 &in direction = vector2(0.0f, 0.0f))
	{
		const int id = AddEntity("CoconutTest.ent", vector3(position, 0.0f), "Coconut");
		@m_entity = SeekEntity(id);
		m_direction = direction;
	}

	bool update()
	{
		m_entity.AddToPositionXY(m_direction * UnitsPerSecond(m_moveSpeed));

		if(m_entity.GetPosition().x >= GetScreenSize().x ||
			m_entity.GetPosition().y >= GetScreenSize().y ||
			m_entity.GetPosition().x <= 0 ||
			m_entity.GetPosition().y <= 0)
		{
			DeleteEntity(m_entity);
			return false;
		}
		else
		{
			return true;
		}
	}
}

void ETHBeginContactCallback_Coconut(
	ETHEntity@ thisEntity,
	ETHEntity@ other,
	vector2 cA,
	vector2 cB,
	vector2 cN)
{
	if(other.GetEntityName() == "Enemy")
	{
		
	}
}