﻿#include "CharacterController.angelscript"

class MainCharacterController : CharacterController
{
	private vector2 m_movementDirection = vector2(0.0f, 0.0f);
	private bool m_shoot = false;

	void update()
	{
		m_movementDirection = vector2(0.0f, 0.0f);
		m_shoot = false;
		movement();
		shoot();
	}

	vector2 getMovementDirection() const
	{
		return m_movementDirection; 
	}

	bool getShoot() const
	{
		return m_shoot;
	}

	private void movement()
	{
		ETHInput@ input = GetInputHandle();

		if(input.KeyDown(K_W))
		{
			m_movementDirection.y -= 1.0f;
		}
		if(input.KeyDown(K_S))
		{
			m_movementDirection.y += 1.0f;
		}
		if(input.KeyDown(K_A))
		{
			m_movementDirection.x -= 1.0f;
		}
		if(input.KeyDown(K_D))
		{
			m_movementDirection.x += 1.0f;
		}
	}

	private void shoot()
	{
		ETHInput@ input = GetInputHandle();

		if(input.GetKeyState(K_SPACE) == KS_HIT)
		{
			m_shoot = true;
		}
	}
}
