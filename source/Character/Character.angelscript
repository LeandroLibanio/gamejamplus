﻿#include "MainCharacterController.angelscript"
#include "../Animation/FrameTimer.angelscript"
#include "Banana.angelscript"
#include "Coconut.angelscript"

class Character
{
	ETHEntity@ m_entity;

	private FrameTimer m_frameTimer;
	private uint m_directionLine = 1;
	private uint m_frameColumn = 0;

	private array<Bullet@> m_bullets;

	private float m_moveSpeed = 200.0f;
	private float m_timeSinceLastShoot = 1.0f;

	private vector2 m_direction = vector2(0.0f, 1.0f);

	private bool m_coconut = false;

	Character(const string &in entityName, const vector2 pos)
	{
		// add character entity and rename it to "character" for matching character
		// specific entity callback functions
		const int id = AddEntity(entityName, vector3(pos, -2.0f), "Character");
		@m_entity = SeekEntity(id);
	}

	void update(CharacterController@ characterController)
	{
		ETHPhysicsController@ physicsController = m_entity.GetPhysicsController();

		// never let character body sleep
		physicsController.SetAwake(true);

		updateMovement(@physicsController, characterController.getMovementDirection());

		updateShoot(characterController.getShoot());

		// update entity animation frame
		//m_entity.SetFrame(m_frameColumn, m_directionLine);

		//Update bananas
		updateBullets();
		//print(m_bullets.length()); --Debug
	}

	private void updateMovement(ETHPhysicsController@ physicsController, const vector2 moveDir)
	{
		float dirX = 0.0f;
		float dirY = 0.0f;		
		// if there's movement, update animation
		if (moveDir != vector2(0.0f, 0.0f))
		{
			if(moveDir.y != 0.0f)
			{
				dirY = moveDir.y > 0? 1.0f : -1.0f;
			}
			else
			{
				dirX = moveDir.x > 0? 1.0f : -1.0f;
			}

			m_direction = vector2(dirX, dirY);

			//Handle
		}
		else
		{	
			//Handle
		}
		physicsController.SetLinearVelocity(moveDir * UnitsPerSecond(m_moveSpeed));
	}

	private void updateShoot(const bool shoot)
	{
		if(m_entity.GetUInt("COCONUT") != 0)
		{
			m_coconut = true;
		}
		if(shoot && m_timeSinceLastShoot >= 1.0f)
		{
			if(!m_coconut)
			{
				m_bullets.insertLast(Banana(m_entity.GetPositionXY(), 
				m_direction));
			}
			else
			{
				m_bullets.insertLast(Coconut(m_entity.GetPositionXY(), 
				m_direction));
				m_coconut = false;
				m_entity.SetUInt("COCONUT", 0);			
			}

			m_timeSinceLastShoot = 0.0f;
		}
		else if(m_timeSinceLastShoot < 1.0f)
		{
			m_timeSinceLastShoot += UnitsPerSecond(1.0f);
		}
	}

	private void updateBullets()
	{
		for(uint i = 0; i < m_bullets.length(); i++)
		{
			if(!m_bullets[i].update())
			{
				m_bullets.removeAt(i);
			}
		}
	}

	vector2 getPosition()
	{
		return m_entity.GetPositionXY();
	}
}

void ETHBeginContactCallback_Character(
	ETHEntity@ thisEntity,
	ETHEntity@ other,
	vector2 cA,
	vector2 cB,
	vector2 cN)
{
	if(other.GetEntityName() == "CoconutPickup")
	{
		if(other.GetUInt("ACTIVE") != 0)
		{
			if(thisEntity.GetUInt("COCONUT") != 1)
			{
				thisEntity.SetUInt("COCONUT", 1);
				other.SetUInt("DESTROY", 1);
			}
		}
		else
		{
			//Damage Player
			//other.SetUInt("DESTROY", 1);
		}
	}
}

void ETHCallback_CoconutPickup(ETHEntity@ thisEntity)
{
	if(thisEntity.GetUInt("DESTROY") != 0)
	{
		DeleteEntity(thisEntity);
	}
}