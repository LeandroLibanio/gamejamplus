﻿interface CharacterController
{
	void update();
	vector2 getMovementDirection() const;
	bool getShoot() const;
}
