#include "Bullet.angelscript"

class Banana : Bullet
{
	private ETHEntity@ m_entity;
	private float m_moveSpeed = 800.0f;
	private FrameTimer m_frameTimer;

	private vector2 m_direction;

	private uint frameLine;
	private uint frameCol;

	Banana(const vector2 &in position = vector2(0.0f, 0.0f), 
		const vector2 &in direction = vector2(0.0f, 0.0f))
	{
		const int id = AddEntity("BananaTest.ent", vector3(position, 0.0f), "Banana");
		@m_entity = SeekEntity(id);
		m_direction = direction;
	}

	bool update()
	{
		m_entity.AddToPositionXY(m_direction * UnitsPerSecond(m_moveSpeed));

		if(m_entity.GetPosition().x >= GetScreenSize().x ||
			m_entity.GetPosition().y >= GetScreenSize().y ||
			m_entity.GetPosition().x <= 0 ||
			m_entity.GetPosition().y <= 0 ||
			m_entity.GetUInt("DESTROY") == 1)
		{
			DeleteEntity(m_entity);
			//print("Deletou");
			return false;
		}
		else
		{
			return true;
		}
	}
}

void ETHBeginContactCallback_Banana(
	ETHEntity@ thisEntity,
	ETHEntity@ other,
	vector2 contactA,
	vector2 contactB,
	vector2 contactNormal)
{
	//print("oi");
	if(other.GetEntityName() == "Trunk.ent" ||
		other.GetEntityName() == "Rock.ent" ||
		other.GetEntityName() == "RockShadow.ent" ||
		other.GetEntityName() == "Bush.ent" ||
		other.GetEntityName() == "BushSmall.ent")
	{
		//print("oi");
		thisEntity.SetUInt("DESTROY", 1);
	}		
}