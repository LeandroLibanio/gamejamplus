class CoconutPickup
{
	private ETHEntity@ m_entity;
	private vector2 m_destiny;

	CoconutPickup(vector2 pos, vector2 dest)
	{
		const int id = AddEntity("CoconutPickup.ent", 
			vector3(pos, 0.0f), "CoconutPickup");
		@m_entity = SeekEntity(id);
		m_destiny = dest;

		m_entity.SetUInt("ACTIVE", 0);
	}

	bool update()
	{
		if(m_entity.GetUInt("ACTIVE") == 0)
		{
			vector2 offset = normalize((m_destiny + vector2(0, 10)) - m_entity.GetPositionXY());
			m_entity.AddToPositionXY(offset * UnitsPerSecond(300));

			if(m_entity.GetPositionY() >= m_destiny.y)
			{
				m_entity.SetUInt("ACTIVE", 1);
				return false;
			}
		}

		if(m_entity.GetUInt("DESTROY") != 0)
		{
			return false;
		}

		return true;
	}
}