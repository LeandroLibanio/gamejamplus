class Slot
{
	private bool m_active = false;
	private ETHEntity@ m_entity;

	Slot(const vector2 &in position = vector2(0.0f, 0.0f))
	{
		const int id = AddEntity("SlotTest.ent", vector3(position, 0.0f), "Slot");
		@m_entity = SeekEntity(id);
		m_entity.SetUInt("ACTIVE", 0);
	}

	bool update()
	{
		if(m_entity.GetUInt("ACTIVE") != 0)
		{
			m_active = true;
		}
		return m_active;
	}
}

void ETHBeginContactCallback_Slot
(ETHEntity@ thisEntity,
	ETHEntity@ other,
	vector2 cpa,
	vector2 cpb,
	vector2 cn)
{
	if(other.GetEntityName() == "Box")
	{
		thisEntity.SetUInt("ACTIVE", 1);
	}
}