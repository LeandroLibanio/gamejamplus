class Rhinoceros
{
	private ETHEntity@ m_entity;

	private array<vector2> m_wayPoints;
	private array<vector2> m_aux;
	private int m_currentWayPoint = 0;

	private vector2 m_nextPosition;

	private bool going = true;

	Rhinoceros(array<vector2> wayPoints)
	{
		m_wayPoints = wayPoints;
		m_aux = wayPoints;
		m_aux.reverse();

		for(int i = 0; i < m_aux.length(); i++)
		{
			m_wayPoints.insertLast(m_aux[i]);
		}

		const int id = AddEntity("RhinocerosTest.ent",
			vector3((m_wayPoints[0] * 128) + (vector2(64, 64)), 0.0f) * GetScale(), "Enemy");
		@m_entity = SeekEntity(id);
	}

	bool update()
	{
		movement();
		return true;
	}

	private void movement()
	{
		const vector2 currentBucket = m_entity.GetCurrentBucket();
		print(vector2ToString(currentBucket));
		if(currentBucket == m_wayPoints[m_currentWayPoint])
		{
			m_currentWayPoint ++;
			if(m_currentWayPoint >= m_wayPoints.length())
			{
				m_currentWayPoint = 0;
			}

			m_nextPosition = m_wayPoints[m_currentWayPoint] * (128.0f * GetScale());
			m_nextPosition += vector2(64.0f, 64.0f) * GetScale();

		}
		else
		{
			vector2 offset = normalize(m_nextPosition - 
				m_entity.GetPositionXY());

			m_entity.AddToPositionXY(offset * UnitsPerSecond(200.0f));
		}
	}
}