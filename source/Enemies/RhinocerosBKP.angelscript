class Rhinoceros
{
	private ETHEntity@ m_entity;

	private FrameTimer m_frameTimer;

	//Controll variables for movement
	private vector2 m_start;
	private vector2 m_destiny;
	private vector2 m_direction = vector2(0.0f, 0.0f);

	//Current target destiny
	private vector2 m_currentDestiny;

	//How fast can the rhinoceros move?
	private float m_moveSpeed = 200.0f;

	//What frame should be rendered?
	private uint m_frameLine;
	private uint m_frameCol;

	//Is the rhinoceros going for 'm_destiny' or regreting for 'm_start'
	private bool m_going = true;

	/*Only change 1 value between x and y for position and destiny*/
	Rhinoceros(const vector2 &in position = vector2(0.0f, 0.0f),
		const vector2 &in destiny = vector2(0.0f, 0.0f))
	{
		const int id = AddEntity("RhinocerosTest.ent", vector3(position, 0.0f), "Enemy");
		@m_entity = SeekEntity(id);

		m_start = position;
		m_destiny = destiny;

		if(m_start.x != m_destiny.x)
		{
			m_direction = 
			m_start.x < m_destiny.x ? vector2(1.0f, 0.0f) : vector2(-1.0f, 0.0f);
		}

		else if(m_start.y != m_destiny.y)
		{
			m_direction = 
			m_start.y < m_destiny.y ? vector2(0.0f, 1.0f) : vector2(0.0f, -1.0f);
		}

		m_currentDestiny = destiny;
	}

	bool update()
	{
		move();
		return true;
	}

	private void move()
	{	
		//Some messy and error-prone code here
		//I really don't know how I've written that
		//But it's working fine
		//And that's all what matters
		if(m_direction.x != 0.0f)
		{
			if(m_direction.x < 0)
			{
				if(m_entity.GetPositionX() > m_currentDestiny.x)
				{
					m_entity.AddToPositionXY
					(m_direction * UnitsPerSecond(m_moveSpeed));
				}
				else
				{
					m_direction *= -1;
					m_currentDestiny = m_destiny == m_currentDestiny ? m_start : m_destiny;
				}
			}
			else
			{
				if(m_entity.GetPositionX() < m_currentDestiny.x)
				{
					m_entity.AddToPositionXY
					(m_direction * UnitsPerSecond(m_moveSpeed));
				}
				else
				{
					m_direction *= -1;
					m_currentDestiny = m_destiny == m_currentDestiny ? m_start : m_destiny;
				}
			}
		}
		else if(m_direction.y != 0.0f)
		{
			if(m_direction.y < 0)
			{
				if(m_entity.GetPositionY() > m_currentDestiny.y)
				{
					m_entity.AddToPositionXY
					(m_direction * UnitsPerSecond(m_moveSpeed));
				}
				else
				{
					m_direction *= -1;
					m_currentDestiny = m_destiny == m_currentDestiny ? m_start : m_destiny;
				}
			}
			else
			{
				if(m_entity.GetPositionY() < m_currentDestiny.y)
				{
					m_entity.AddToPositionXY
					(m_direction * UnitsPerSecond(m_moveSpeed));
				}
				else
				{
					m_direction *= -1;
					m_currentDestiny = m_destiny == m_currentDestiny ? m_start : m_destiny;
				}
			}
		}
	}
}