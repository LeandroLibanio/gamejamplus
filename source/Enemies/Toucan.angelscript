#include "../Objects/CoconutPickup.angelscript"

class Toucan
{
	private ETHEntity@ m_entity;
	private float m_moveSpeed = 400.0f;
	private float m_currentTime;
	private vector2 m_direction;
	private float m_cocunutTime;

	private array<CoconutPickup@> m_coconuts;

	Toucan()
	{
		const int id = AddEntity("Toucan.ent", 
			vector3(-50.0f * GetScale(), 100.0f * GetScale(), 0.0f) * GetScale(),
			"Toucan");

		@m_entity = SeekEntity(id);
		m_currentTime = 0.0f;
		m_direction = vector2(1.0f, 0.0f);
	}

	void update(vector2 target)
	{
		m_cocunutTime += UnitsPerSecond(1.0f);
		if(m_currentTime >= 14.0f)
		{
			m_entity.AddToPositionXY(

				m_direction * UnitsPerSecond(m_moveSpeed));

			if(m_entity.GetPositionX() > GetScreenSize().x + (50.0f * GetScale()))
			{
				m_entity.SetPositionXY(vector2(GetScreenSize().x + (45.0f * GetScale()), 100 * GetScale()));
				m_currentTime = 0.0f;
				m_direction *= -1;
			}
			else if(m_entity.GetPositionX() < -50 * GetScale())
			{
				m_entity.SetPositionXY(vector2(-45 * GetScale(), 100 * GetScale()));
				m_currentTime = 0.0f;
				m_direction *= -1;
			}

			if(m_entity.GetPositionX() >= target.x - 30 * GetScale() &&
				m_entity.GetPositionX() <= target.x + 30 * GetScale() &&
				m_cocunutTime >= 0.5f)
			{
				m_coconuts.insertLast(CoconutPickup(m_entity.GetPositionXY(), target));
				m_cocunutTime = 0.0f;
			}
		}
		else
		{
			m_currentTime += UnitsPerSecond(1.0f);
		}

		for(uint i = 0; i < m_coconuts.length(); i++)
		{
			if(!m_coconuts[i].update())
			{
				m_coconuts.removeAt(i);
			}
		}
	}
}