/*Scene for tests*/

#include "Scene.angelscript"
#include "../Character/Character.angelscript"
#include "../Enemies/Rhinoceros.angelscript"
#include "../Objects/Slot.angelscript"
#include "../Enemies/Toucan.angelscript"

class SceneTest : Scene
{
	private Character@ m_character;
	private MainCharacterController m_characterController;
	private Rhinoceros@ m_rhinoceros;
	private Toucan@ m_toucan;

	private array<Slot@> m_slots;

	private array<vector2> wayPoints = {
		vector2(14, 5),
		vector2(12, 12)
	};

	SceneTest(const string &in sceneName = "empty")
	{
		//Set scene name if not "empty"
		super(sceneName);
	}

	void onCreated() override
	{
		//Get screen Middle
		const vector2 screenMiddle(GetScreenSize() * 0.5f);
		//Call 'Character' constructor
		@m_character = Character("MonkeyTest.ent", screenMiddle);
		/*@m_rhinoceros = Rhinoceros(vector2(screenMiddle.x + 100, 100),
			vector2(screenMiddle.x + 300, 100));*/
		@m_rhinoceros = Rhinoceros(wayPoints);
		@m_toucan = Toucan();

		m_slots.insertLast(Slot(vector2(screenMiddle.x + 200, screenMiddle.y)));
		AddEntity("BoxTest.ent", vector3(screenMiddle.x + 70, screenMiddle.y, 0.0f), "Box");
	}

	void onUpdate() override
	{
		/*Update character controller in order to handle
		player input*/
		m_characterController.update();

		/*Update the character itself passing a character
		controller reference*/
		m_character.update(@m_characterController);
		bool a = m_rhinoceros.update();
		m_toucan.update(m_character.getPosition());

		for(int i = 0; i < m_slots.length(); i++)
		{
			bool canPass = true;
			if(!m_slots[i].update())
			{
				canPass = false;
			}

			if(canPass)
			{
				//print("Can Pass");
			}
			else
			{
				//print("Cannot pass");
			}			
		}

		//print(GetFPSRate());
	}
}