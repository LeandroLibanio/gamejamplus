﻿/*Entry Point for the game*/

#include "source/Scenes/SceneManager.angelscript"
#include "source/Scenes/SceneTest.angelscript"
#include "eth_util.angelscript"

void main()
{
	/*Set scale factor to fit the entire screen using 
	full-hd resolution as reference*/
	//The game will only work on 16:9 resolutions
	float scaleFactor = GetScreenSize().x / 1920.0f;
	SetScaleFactor(scaleFactor);

	//Load the test scene
	g_sceneManager.setCurrentScene(
		SceneTest("scenes/Scene1.esc"));
}